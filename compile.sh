#!/bin/bash

APPNAME="uk.co.retallack.camera_3.1.5"

rm ${APPNAME}_arm64.click

docker build --rm -t imagetest .


containerID=`docker create imagetest`

docker cp $containerID:/build/build/aarch64-linux-gnu/app/${APPNAME}_arm64.click .

docker rm $containerID

scp ${APPNAME}_arm64.click PinePhoneUSB:~/

ssh PinePhoneUSB pkcon --allow-untrusted install-local ${APPNAME}_arm64.click



